/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

import java.util.Iterator;

/**
 *  Wrapper (Envoltorio)
 * @author docente
 */
public class Conjunto<T> implements IConjunto<T>, Iterable<T>{
    
    private ListaS<T> elementos = new ListaS();

    public Conjunto() {
    }
    
    @Override
    public Conjunto<T> getUnion(Conjunto<T> c1) {
        Conjunto<T> c2 = new Conjunto();
        for(T elem:this.elementos){
            if(!c2.contains(elem)){
                c2.insertar(elem);
            }
        }
        for(T elem:c1.elementos){
            if(!c2.contains(elem)){
                c2.insertar(elem);
            }
        }
        return c2;
    }

    @Override
    public Conjunto<T> getInterseccion(Conjunto<T> c1) {
        Conjunto<T> c2 = new Conjunto();
        for(T e:this.elementos){
            for(T e2:c1.elementos){
                if(e.equals(e2)){
                    c2.insertar(e2);
                }
            }
        }
        return c2;
    }

    @Override
    public Conjunto<T> getDiferenciaAsimetrica(Conjunto<T> c1) {
        Conjunto<T> c2 = new Conjunto();
        for(T elem: this.elementos){
            if(!c1.contains(elem)){
                c2.insertar(elem);
            }
        }
        return c2;
    }

    @Override
    public Conjunto<T> getDiferencia(Conjunto<T> c1) {
        Conjunto<T> c2 = new Conjunto();
        Conjunto<T> caux = this.getInterseccion(c1);
        for(T elem:this.elementos){
            if(!caux.contains(elem)){
                c2.insertar(elem);
            }
        }
        for(T elem:c1.elementos){
            if(!caux.contains(elem)){
                c2.insertar(elem);
            }
        }
        return c2;
    }

    public Conjunto<Conjunto<T>> getPares() {
        if(elementos.getSize()<2) throw new RuntimeException("tamaño insuficiente");
        Conjunto<Conjunto<T>> c1 = new Conjunto();
        Conjunto<T> tmp;
        int t = 1, i;
        for(T elem:this.elementos){
            t++;
            i=1;
            for(T elem2: this.elementos){
                if(i>=t){
                tmp = new Conjunto();
                tmp.insertar(elem);
                tmp.insertar(elem2);
                c1.insertar(tmp);
                }else{
                    
                }
                i++;
            }
        }
        return c1;
    }
    
    
    
    public boolean contains(T x){
        if(this.elementos.getSize()==0){
            return false;
        }
        for(T elem:this.elementos){
            if(elem.equals(x)){
                return true;
            }
        }
        return false;
    }
    
    @Override
    public T get(int i) {
        return elementos.get(i);
    }

    @Override
    public void set(int i, T info) {
        elementos.set(i, info);
    }

    @Override
    public void insertar(T info) {
        elementos.insertarFin(info);
    }

    @Override
    public Conjunto<Conjunto<T>> getPotencia() {
        Conjunto<Conjunto<T>> c1 = new Conjunto<>();
        int n = this.elementos.getSize();
        int tot = 1 << n;
        for (int i = 0; i < tot; i++) {
            Conjunto<T> tmp = new Conjunto<>();
            for (int j = 0; j < n; j++) {
                if ((i & (1 << j)) != 0) {
                    T elemento = this.get(j);
                    tmp.insertar(elemento);
                }
            }
            c1.insertar(tmp);
        }
        return c1;
    }
    
    public String toString(){
        return this.elementos.toString();
    }
    
    @Override
    public Iterator<T> iterator() {
        return elementos.iterator();
    }
}
