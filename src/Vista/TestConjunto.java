/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;
import Util.seed.Conjunto;
/**
 *
 * @author camil
 */
public class TestConjunto {
    public static void main(String[] args) {
        Conjunto<Integer> x = new Conjunto();
        x.insertar(1);
        x.insertar(2);
        x.insertar(3);
        x.insertar(4);
        x.insertar(5);
        Conjunto<Integer> y = new Conjunto();
        y.insertar(1);
        y.insertar(2);
        y.insertar(3);
        y.insertar(6);
        y.insertar(7);
        System.out.println(x.getDiferencia(y));
        System.out.println(x.getPotencia());
        System.out.println(x.getDiferenciaAsimetrica(y));
        System.out.println(x.getInterseccion(y));
        System.out.println(x.getPares());
        System.out.println(x.getUnion(y));
    }
}
