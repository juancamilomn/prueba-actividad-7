/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;
import Util.seed.Pila;
import java.util.Scanner;
/**
 *
 * @author DOCENTE
 */
public class TestBalanceoSignos {
    public static void main(String[] args) {
        System.out.println("Validando [{()}] ");
        /**
         * Ejemplo: L={aa} => --> V
         * L={[aa]} => --> F
         * L=(a)[a]{b}--> V
         */
        boolean cumple = true;
        Scanner t = new Scanner(System.in);
        String s = t.nextLine();
        Pila<Character> p = new Pila();
        try{
        for (int i = 0; i < s.length()&&cumple; i++) {
            switch (s.charAt(i)) {
                case '[':if(p.isVacia()||p.peek()=='['){
                    p.push('[');
                    break;
                }
                    cumple = false;
                case ']':
                    if(p.pop()!='['){
                        cumple = false;
                    }
                    break;
                case '{':if(p.isVacia()||p.peek()=='['||p.peek()=='{'){
                    p.push('{');
                    break;
                }
                    cumple = false;
                case '}':
                    if(p.pop()!='{'){
                        cumple = false;
                    }
                    break;
                case '(':
                    p.push('(');
                    break;
                case ')':
                    if(p.pop()!='('){
                         cumple = false;
                    }
                    break;
                default:
                    break;
            }
        }
        }catch (Exception e){
            cumple = false;
        }
        if(p.isVacia() && cumple){
            System.out.println("cumple");
        }else{
            System.out.println("no cumple");
        }
    }
}
