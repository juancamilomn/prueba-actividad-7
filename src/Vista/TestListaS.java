/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Util.seed.ListaS;

/**
 *
 * @author Estudiantes
 */
public class TestListaS {

    public static void main(String[] args) {

        //Clase parametrizada : Integer
        ListaS<Integer> lista = new ListaS();
        lista.insertarInicio(4);
        lista.insertarInicio(5);
        lista.insertarInicio(6);
        lista.insertarFin(7);
        
         ListaS<Integer> lista2 = new ListaS();
        lista2.insertarInicio(4);
        lista2.insertarInicio(5);
        lista2.insertarInicio(6);
        lista2.insertarFin(7);
        
        System.out.println(lista.toString());
        System.out.println("Pos 2:" + lista.get(2));
        lista.set(2, -100);
        System.out.println(lista.toString());
        
        // un ejemplo en extremo CANDIDO :(
        //simulo un vector
        for(int i=0;i<lista.getSize();i++)
            System.out.println("Pos("+i+"):"+lista.get(i));
        
        
        //Concatenando:
        
        lista.addAll(lista2);
        System.out.println("Concatenando:");
        System.out.println(lista);
        System.out.println(lista2);
        

    }
    
    
    
    
}
