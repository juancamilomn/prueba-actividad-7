/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.ListaS;
import java.util.Random;

/**
 *
 * @author docente
 */
public class TestListaIterator {

    public static void main(String[] args) {

        ListaS<Integer> l1 = crear(100000);
        //sumaCandida(l1);
        sumaBacana(l1);
    }

    private static void sumaBacana(ListaS<Integer> l) {
        long s = 0;
        for (Integer x:l) {
            s += x; //--> llama n veces a getPos() y en cada iteración recorre desde la cabeza
        }
        System.out.println("La suma es:" + s);
    }

    private static void sumaCandida(ListaS<Integer> l) {
        long s = 0;
        for (int i = 0; i < l.getSize(); i++) {
            s += l.get(i); //--> llama n veces a getPos() y en cada iteración recorre desde la cabeza
        }
        System.out.println("La suma es:" + s);
    }

    private static ListaS<Integer> crear(int n) {

        if (n <= 0) {
            throw new RuntimeException("No puedo crear lista");
        }
        ListaS<Integer> l = new ListaS();
        while (n > 0) {
            l.insertarInicio(new Random().nextInt(n));
            n--;
        }
        return l;
    }

}
